#!/bin/bash

# Exit immediately if a command exits with a non-zero status
set -e

###########################
# Try to start routersploit
###########################

test_start() {

    echo "Start routersploit"
    cat <<EOF
#!/usr/bin/expect
spawn routersploit
expect "rsf >"
send "help\n"
expect "Global commands:"
send "exit"
EOF

}

#########################
# Main
#########################

for function in "$@"; do
        $function
done

